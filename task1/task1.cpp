#include "Word.hpp"

#include <cerrno>
#include <cctype>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sstream>
#include <stdexcept>

#define INPUT_FNAME	"input"
#define OUTPUT_FNAME	"output"

int main(void) {
	std::ifstream in;
	in.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try {
		in.open(INPUT_FNAME);
	} catch (const std::exception& e) {
		std::ostringstream msg;
		msg
			<< "Failed to open file 'input' for reading: "
			<< strerror(errno);
		throw std::runtime_error(msg.str());
	}

	std::ofstream out;
	out.exceptions(std::ofstream::failbit | std::ofstream::badbit);
	try {
		out.open(OUTPUT_FNAME);
	} catch (const std::exception& e) {
		std::ostringstream msg;
		msg
			<< "Failed to open file 'output' for writing: "
			<< strerror(errno);
		throw std::runtime_error(msg.str());
	}

	char ch;

	try {
		while (true) {
			in >> ch;
			in.unget();

			if (std::isdigit(ch)) {
				Word<int> w(in);
				w.print_answer(out);
			} else if (std::isalpha(ch)) {
				Word<char> w(in);
				w.print_answer(out);
			} else {
				std::ostringstream msg;
				msg
					<< "Unexpected character: (ascii code: "
					<< +ch << ") "
					<< ch;
				throw std::runtime_error(msg.str());
			}
			out << " ";
		}
	} catch (const std::fstream::failure& e) {
		out << std::endl;

		if (in.eof())
			return 0;

		std::ostringstream msg;
		msg << "input/output fail: " << strerror(errno);
		throw std::runtime_error(msg.str());
	}
}
