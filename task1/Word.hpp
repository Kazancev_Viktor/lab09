#ifndef WORD_HPP
#define WORD_HPP

#include <algorithm>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <memory>
#include <stdexcept>

#define DEFAULT_ALLOC_SIZE	120

template <typename T>
struct Word {
	std::unique_ptr<T []> str;
	size_t alloced, length = 0;

	Word(std::istream &in);

	void print_answer(std::ostream &out) {
		if (palindrom()) {
			out << max();
			return;
		} else if (progression()) {
			out << mid();
			return;
		}
		print(out);
	}

	void print(std::ostream &out);

	bool palindrom(void) {
		for (size_t i = 0; i < length / 2; i++)
			if (str[i] != str[length - i - 1])
				return false;
		return true;
	}

	bool progression(void) {
		if (length < 2)
			return false;
		int diff = str[1] - str[0];
		if (diff <= 0)
			return false;
		for (size_t i = 2; i < length; i++)
			if (str[i] - str[i - 1] != diff)
				return false;
		return true;
	}

	T max(void) {
		T max = str[0];
		for (size_t i = 1; i < length; i++)
			max = std::max(max, str[i]);
		return max;
	}

	T mid(void);
};

template<>
Word<char>::Word(std::istream &in) {
	std::ios_base::fmtflags previos_flags = in.flags();

	if (!str) {
		alloced = DEFAULT_ALLOC_SIZE;
		str.reset(new char[alloced]);
	}

	length = 0;
	char ch;

	in >> std::skipws >> ch >> std::noskipws;

	do {
		if (std::isspace(ch))
			break;

		if (!std::isalpha(ch)) {
			std::ostringstream msg;
			msg
				<< "Unexpected character: (ascii code: "
				<< +ch
				<< ") "
				<< ch;
			throw std::runtime_error(msg.str());
		}

		if (length == alloced - 1) {
			alloced *= 2;
			std::unique_ptr<char []> new_str(new char[alloced]);
			memcpy(new_str.get(), str.get(), length * sizeof(char));
			str = std::move(new_str);
		}

		str[length++] = ch;
	} while (in >> ch);

	str[length] = '\0';

	in >> std::skipws;
	in.flags(previos_flags);
}

template<>
Word<int>::Word(std::istream &in) {
	std::ios_base::fmtflags previos_flags = in.flags();

	if (!str) {
		alloced = DEFAULT_ALLOC_SIZE;
		str.reset(new int[alloced]);
	}

	length = 0;
	char ch;

	in >> std::skipws >> ch >> std::noskipws;

	do {
		if (std::isspace(ch))
			break;

		if (!std::isdigit(ch)) {
			std::ostringstream msg;
			msg
				<< "Unexpected character: (ascii code: "
				<< +ch
				<< ") "
				<< ch;
			throw std::runtime_error(msg.str());
		}

		if (length == alloced) {
			alloced *= 2;
			std::unique_ptr<int []> new_str(new int[alloced]);
			memcpy(new_str.get(), str.get(), length * sizeof(int));
			str = std::move(new_str);
		}

		str[length] = (ch - '0') * 10;

		if (in.eof())
			throw std::runtime_error("Unexpected EOF");
		ch = in.get();

		if (!std::isdigit(ch)) {
			std::ostringstream msg;
			msg
				<< "Unexpected character: (ascii code: "
				<< +ch
				<< ") "
				<< ch;
			throw std::runtime_error(msg.str());
		}

		str[length++] += ch - '0';
	} while (in >> ch);

	in >> std::skipws;
	in.flags(previos_flags);
}

template<>
void Word<char>::print(std::ostream &out) {
	out << str.get();
}

template<>
void Word<int>::print(std::ostream &out) {
	std::ios_base::fmtflags previos_flags = out.flags();
	for (size_t i = 0; i < length; i++)
		out << std::setfill('0') << std::setw(2) << str[i];
	out.flags(previos_flags);
}

template<>
char Word<char>::mid(void) {
	return str[length / 2];
}

template<>
int Word<int>::mid(void) {
	int sum = 0;
	for (size_t i = 0; i < length; i++)
		sum += str[i];
	return sum / length;
}

#endif // WORD_HPP
