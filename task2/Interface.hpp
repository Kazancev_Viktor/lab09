#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include "Baker.hpp"
#include "move.hpp"

class Baker; // Forward declaration (cross-reference)

class Interface {
	public:
		Interface(void);
		Interface(const Interface &other);
		const Interface &operator=(const Interface &other);
		move getMove(const Baker &baker) const;
		~Interface(void);

};

#endif // INTERFACE_HPP
