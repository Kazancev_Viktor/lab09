#include "Team.hpp"
#include "Baker.hpp"

#include <exception>
#include <iostream>

Team::Team(Baker &player1, Baker &player2) {
	std::cout << "Team::Team(Baker &player1, Baker &player2)" << std::endl;

	player1.setTeammate(player2);
	player2.setTeammate(player1);

	baker.at(0) = &player1;
	baker.at(1) = &player2;

	made_a_move.fill(false);
}

Team::Team(const Team &other) {
	std::cout << "Team::Team(const Team &other)" << std::endl;
}

Baker &Team::getBaker(void) {
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++)
			if (!made_a_move.at(j)) {
				made_a_move.at(j) = true;
				return *baker.at(j);
			}
		made_a_move.fill(false);
	}

	throw std::runtime_error("Team::getBacker doesn't work properly");
}

const Team &Team::operator=(Team &other) {
	std::cout << "const Team &operator=(const Team &other)" << std::endl;
	baker.at(0) = &other.getBaker();
	baker.at(1) = &other.getBaker();

	made_a_move.fill(false);

	return *this;
}

Team::~Team(void) {
	std::cout << "Team::~Team(void)" << std::endl;
}
