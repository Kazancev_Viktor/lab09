#include "Baker.hpp"
#include "Interface.hpp"
#include "Team.hpp"
#include "game_config.hpp"

#include <cstdlib>
#include <ctime>
#include <iostream>

int Bake(Baker player1, Baker player2) {
	Team Team(player1, player2);
	for (int i = 0; i < BAKE_CYCLES; i++)
		Team.getBaker().makeMove();
	return 0;
}

int main(){
	std::srand(std::time(nullptr));

	Interface interface;
	Baker player1("Player 1", &interface), player2("Player 2", &interface);
	int exp = Bake(player1, player2);

	player1.addExp(exp);
	player2.addExp(exp);

	return 0;
}
