#include "Pie.hpp"
#include "game_config.hpp"

#include <algorithm>
#include <exception>
#include <iostream>

int Pie::_count = 0;

Pie::Pie(void) {
	std::cout << "Pie::Pie(void)" << std::endl;

	_name = pie_default_names[rand() % PIE_DEFAULT_NAMES_N];

	_taste_lvl
		=
		TASTE_LVL_MIN
		+
		std::rand()
		%
		(
		 TASTE_LVL_MAX
		 +
		 1
		 -
		 TASTE_LVL_MIN
		);

	_count++;
}

Pie::Pie(const std::string &name, const int taste_lvl):_name(name) {
	std::cout
		<< "Pie::Pie(const std::string &name, const int taste_lvl)"
		<< std::endl;

	if (taste_lvl < TASTE_LVL_MIN || taste_lvl > TASTE_LVL_MAX)
		throw std::runtime_error("Taste level out of range");
	_taste_lvl = taste_lvl;

	_count++;
}

Pie::Pie(const std::string &name):_name(name) {
	std::cout << "Pie::Pie(const std::string &name)" << std::endl;

	_taste_lvl
		=
		TASTE_LVL_MIN
		+
		std::rand()
		%
		(
		 TASTE_LVL_MAX
		 +
		 1
		 -
		 TASTE_LVL_MIN
		);

	_count++;
}

Pie::Pie(const int taste_lvl) {
	std::cout << "Pie::Pie(const int taste_lvl)" << std::endl;

	_name = pie_default_names[rand() % PIE_DEFAULT_NAMES_N];

	if (taste_lvl < TASTE_LVL_MIN || taste_lvl > TASTE_LVL_MAX)
		throw std::runtime_error("Taste level out of range");

	_taste_lvl = taste_lvl;

	_count++;
}

Pie::Pie(const Pie &other) {
	std::cout << "Pie::Pie(const Pie &other)" << std::endl;
	*this = other;
	_count++;
}

Pie::~Pie(void) {
	std::cout << "Pie::~Pie(void)" << std::endl;

	_count--;
}

int Pie::getCount(void) {
	return _count;
}

const std::string &Pie::getName(void) const {
	return _name;
}

int Pie::getTasteLvl(void) const {
	return _taste_lvl;
}

Pie Pie::theMostDelicious(const Pie &a, const Pie &b) {
	return std::max(a, b);
}

bool Pie::operator<(const Pie &other) const {
	return _taste_lvl < other.getTasteLvl();
}

Pie &Pie::operator=(const Pie &other){
	std::cout << "Pie &Pie::operator=(const Pie &other)" << std::endl;

	_name = other.getName();
	if
		(
		 other.getTasteLvl() < TASTE_LVL_MIN
		 ||
		 other.getTasteLvl() > TASTE_LVL_MAX
		)
		throw std::runtime_error("Taste level out of range");
	_taste_lvl = other.getTasteLvl();
	return *this;
}
