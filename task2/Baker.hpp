#ifndef BAKER_HPP
#define BAKER_HPP

#include "Interface.hpp"
#include "Pie.hpp"

#include <memory>
#include <string>

#define DEFAULT_ALLOC_SIZE	120

class Interface; // Forward declaration (cross-reference)

class Baker {
	public:
		Baker(const std::string &name, Interface *interface);
		Baker(const Baker &other);
		Baker &getTeammate(void);
		Baker &operator=(const Baker &other);
		const Interface &getInterface(void) const;
		const Pie &getPie(size_t idx) const;
		const std::string &getName(void) const;
		int getLvl(void) const;
		int getExp(void) const;
		int getPieProgress(void) const;
		int getSatiety(void) const;
		size_t getAlloced(void) const;
		size_t getPiesNumber(void) const;
		void addExp(int value);
		void createPie(void);
		void eatPie(void);
		void givePie(Baker &other);
		void makeMove(void);
		void updateState(void);
		void setTeammate(Baker &Teammate);
		~Baker(void);
	private:
		void extend_pies(void);

		Baker *_Teammate;
		Interface *_interface;
		int _lvl = 1, _exp = 0, _satiety = 100, _piesProgress = 0;
		size_t _alloced = 0, _piesNumber = 0;
		std::string _name;
		std::unique_ptr<std::unique_ptr<Pie> []> _pie;
};

#endif // BAKER_HPP
