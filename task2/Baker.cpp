#include "Baker.hpp"
#include "game_config.hpp"
#include "move.hpp"

#include <exception>
#include <iostream>
#include <memory>

Baker::Baker(const std::string &name, Interface *interface)
	:_interface(interface),_name(name) {
	std::cout << "Baker::Baker(void)" << std::endl;
}

Baker::Baker(const Baker &other) {
	std::cout << "Baker::Baker(const Baker &other)" << std::endl;

	*this = other;
}

Baker &Baker::getTeammate(void) {
	if (!_Teammate)
		throw std::runtime_error("This baker don't have Teammate");
	return *_Teammate;
}

Baker &Baker::operator=(const Baker &other) {
	std::cout << "Baker &Baker::operator=(const Baker &other)" << std::endl;

	_lvl = other.getLvl();
	_exp = other.getExp();
	_satiety = other.getSatiety();

	_alloced = other.getAlloced();
	_piesNumber = other.getPiesNumber();
	_pie.reset(new std::unique_ptr<Pie> [_alloced]);

	for (size_t i = 0;  i < _piesNumber; i++)
		_pie[i].reset(new Pie(other.getPie(i)));

	return *this;
}

const Interface &Baker::getInterface(void) const {
	return *_interface;
}

const Pie &Baker::getPie(size_t idx) const {
	if (idx > _piesNumber)
		throw std::runtime_error("Pie index out of range");
	return *_pie[idx];
}

const std::string &Baker::getName(void) const {
	return _name;
}

int Baker::getLvl(void) const {
	return _lvl;
}

int Baker::getExp(void) const {
	return _exp;
}

int Baker::getPieProgress(void) const {
	return _piesProgress;
}

int Baker::getSatiety(void) const {
	return _satiety;
}

size_t Baker::getAlloced(void) const {
	return _alloced;
}

size_t Baker::getPiesNumber(void) const {
	return _piesNumber;
}

void Baker::addExp(int value) {
	_exp += value;
	if (getExp() >= LEVEL_UP_EXP) {
		_lvl += getExp() / LEVEL_UP_EXP;
		_exp = getExp() % LEVEL_UP_EXP;
	}
}

void Baker::createPie(void) {
	_piesProgress++;
	_satiety--;
	if 
		(
		 getPieProgress()
		 >= DEFAULT_BACKING_TIME / getLvl() - getSatiety()
		)
		{
		_piesProgress = 0;

		extend_pies();
		if (_pie[_piesNumber])
			throw std::runtime_error("Pie already exist in that place");
		_pie[_piesNumber++].reset(new Pie);
		}
}

void Baker::eatPie(void) {
	if (_piesNumber == 0)
		throw std::runtime_error("This backer don't have any pies");

	std::unique_ptr<Pie> cur_pie = std::move(_pie[--_piesNumber]);

	_satiety += cur_pie->getTasteLvl();
}

void Baker::extend_pies(void) {
	if (_piesNumber != _alloced)
		return;
	if (!_alloced)
		_alloced = DEFAULT_ALLOC_SIZE;
	else
		_alloced *= 2;

	std::unique_ptr<std::unique_ptr<Pie> []>
		new_pie(new std::unique_ptr<Pie> [_alloced]);

	for (size_t i = 0;  i < _piesNumber; i++)
		new_pie[i] = std::move(_pie[i]);

	_pie = std::move(new_pie);
}

void Baker::givePie(Baker &other) {
	_satiety--;

	other.extend_pies();

	if (_piesNumber == 0)
		throw std::runtime_error("This backer don't have any pies");
	other._pie[other._piesNumber++] = std::move(_pie[--_piesNumber]);
}

void Baker::makeMove(void) {
	move cur = getInterface().getMove(*this);
}

void Baker::updateState(void) {
	_satiety--;
}

void Baker::setTeammate(Baker &Teammate) {
	_Teammate = &Teammate;
}

Baker::~Baker(void) {
	std::cout << "Baker::~Baker(void)" << std::endl;
}
