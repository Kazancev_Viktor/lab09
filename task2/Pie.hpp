#ifndef PIE_HPP
#define PIE_HPP

#include <array>
#include <string>

#define PIE_DEFAULT_NAMES_N	3

class Pie {
	public:
		Pie(void);
		Pie(const std::string &name, const int taste_lvl);
		Pie(const std::string &name);
		Pie(const int taste_lvl);
		Pie(const Pie &other);

		Pie &operator=(const Pie &other);
		bool operator<(const Pie &other) const;

		const std::string &getName(void) const;
		int getTasteLvl(void) const;
		static Pie theMostDelicious(const Pie &a, const Pie &b);
		static int getCount(void);

		~Pie(void);
	private:
		int _taste_lvl;
		static int _count;
		std::string _name;
		const std::array<std::string, PIE_DEFAULT_NAMES_N>
			pie_default_names = 
			{
			 std::string("Pie with meat"),
			 std::string("Pie with potatoes"),
			 std::string("Pie with cottage cheese and mushrooms")
			}
		;
};

#endif // PIE_HPP
