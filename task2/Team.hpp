#ifndef TEAM_HPP
#define TEAM_HPP

#include <array>
#include "Baker.hpp"

class Team {
	public:
		Team(Baker &player1, Baker &player2);
		Team(const Team &other);
		Baker &getBaker(void);
		const Team &operator=(Team &other);
		~Team(void);
	private:
		std::array<bool, 2> made_a_move;
		std::array<Baker *, 2> baker;
};

#endif // TEAM_HPP
