#include "Interface.hpp"

#include <iostream>

Interface::Interface(void) {
	std::cout << "Interface::Interface(void)" << std::endl;
}

Interface::Interface(const Interface &other) {
	std::cout
		<< "Interface::Interface(const Interface &other)"
		<< std::endl;
	*this = other;
}

const Interface &Interface::operator=(const Interface &other) {
	std::cout
		<< "Interface &Interface::operator=(const Interface &other)"
		<< std::endl;
	return *this;
}

move Interface::getMove(const Baker &baker) const {
	std::cout
		<< "Choose:"
		<< std::endl
		<< "bake"
		<< std::endl;

	if (baker.getPiesNumber())
		std::cout << "eat" << std::endl << "give" << std::endl;

	while (true) {
		std::cout << "Your move: ";

		std::string user_input;
		std::cin >> user_input;

		if (user_input == "bake")
			return BAKE;
		if (user_input  == "eat")
			return EAT;
		if (user_input == "give")
			return GIVE;

		std::cout << "Wrong choice" << std::endl;
	}
}

Interface::~Interface(void) {
	std::cout << "Interface::~Interface(void)" << std::endl;
}
